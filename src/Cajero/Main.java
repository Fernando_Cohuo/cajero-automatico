package Cajero;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Administrador.Administrador;
import Cajero.Cajero;

import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

public class Main extends JFrame {

	private JPanel contentPane;
	TextArea textArea = new TextArea();
	// VALOR PARA NUMERO DE TARJETA RANDOM
	Random aleatorio = new Random();
	int valor = aleatorio.nextInt(1234567890);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// MENU PRINCIPAL DEL PROGRAMA

	public Main() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 234, 343);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnAdmin = new JButton("Administrador");
		btnAdmin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				Administrador admin = new Administrador();
				admin.setVisible(true);

			}
		});
		btnAdmin.setBounds(48, 122, 118, 23);
		contentPane.add(btnAdmin);

		JButton btnCajero = new JButton("Cajero");
		btnCajero.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Cajero cajero = new Cajero();
				cajero.setVisible(true);

			}
		});
		btnCajero.setBounds(48, 63, 118, 23);
		contentPane.add(btnCajero);

		JLabel lblNewLabel = new JLabel("Selecciona una opcion:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel.setBounds(10, 11, 234, 35);
		contentPane.add(lblNewLabel);

		JButton btnSalir = new JButton("Salir");
		btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				System.exit(0);
			}
		});
		btnSalir.setBounds(119, 270, 89, 23);
		contentPane.add(btnSalir);
	}
}