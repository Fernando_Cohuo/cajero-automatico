package Administrador;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ModificarUsuario extends JFrame {

	private JPanel contentPane;
	private JTextField txtID1;
	private JTextField txtNom1;
	private JTextField txtNip1;
	private JTextField txtEfectivo;
	private JLabel lblNewLabel_4;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ModificarUsuario frame = new ModificarUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ModificarUsuario() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 374, 302);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ID: ");
		lblNewLabel.setBounds(10, 74, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre:");
		lblNewLabel_1.setBounds(10, 105, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_3 = new JLabel("Nip: ");
		lblNewLabel_3.setBounds(10, 130, 46, 14);
		contentPane.add(lblNewLabel_3);
		
		txtID1 = new JTextField();
		txtID1.setBounds(87, 71, 86, 20);
		contentPane.add(txtID1);
		txtID1.setColumns(10);
		
		txtNom1 = new JTextField();
		txtNom1.setBounds(87, 102, 86, 20);
		contentPane.add(txtNom1);
		txtNom1.setColumns(10);
		
		txtNip1 = new JTextField();
		txtNip1.setBounds(87, 127, 86, 20);
		contentPane.add(txtNip1);
		txtNip1.setColumns(10);
		
		JButton btnActualizar = new JButton("Actualizar");
		btnActualizar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				MetodosAdmin metodos=new  MetodosAdmin();
				Usuario usuario=new Usuario();
				
				usuario.setIdUsuario(Integer.parseInt(txtID1.getText()));
				usuario= metodos.seleccionarUsuario(usuario);
				
				int id= (usuario.getIdUsuario());
				String Nombre=(txtNom1.getText());
				int numTarjeta=(usuario.getNumTarjeta());
				int NIP=(Integer.parseInt(txtNip1.getText()));
				
				Double fondoInicial=Double.parseDouble(txtEfectivo.getText());
				Double FondoInicial1=(usuario.getFondoInicial());
				Double fondoFinal=fondoInicial;
				
				metodos.actualizarRegistro(id, Nombre, fondoFinal, numTarjeta, NIP);
				JOptionPane.showMessageDialog(null, "Los datos Actualizados son: \n "+ usuario, getTitle(), JOptionPane.WARNING_MESSAGE);
	
				
				
			}
		});
		btnActualizar.setBounds(220, 70, 128, 23);
		contentPane.add(btnActualizar);
		
		txtEfectivo = new JTextField();
		txtEfectivo.setBounds(87, 155, 86, 20);
		contentPane.add(txtEfectivo);
		txtEfectivo.setColumns(10);
		
		lblNewLabel_4 = new JLabel("FondoInicial");
		lblNewLabel_4.setBounds(10, 155, 67, 14);
		contentPane.add(lblNewLabel_4);
		
		btnNewButton = new JButton("Atras");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				Administrador principal = new Administrador();
				principal.setVisible(true);
				
				
			}
		});
		btnNewButton.setBounds(220, 229, 128, 23);
		contentPane.add(btnNewButton);
	}
}