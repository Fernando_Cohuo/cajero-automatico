package Administrador;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

import Cajero.Main;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.TextArea;

public class Administrador extends JFrame {

	private JPanel contentPane;

	// MEN LO CUAL EL USAURIO INTERACTUA AGREGAR, ELIMINAR,ACTUALIZAR,CONSULTAR
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Administrador frame = new Administrador();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Administrador() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 232, 296);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				AgregarUsuario agregar = new AgregarUsuario();
				agregar.setVisible(true);

			}

		});
		btnAgregar.setBounds(60, 26, 89, 23);
		contentPane.add(btnAgregar);

		JButton btnModificar = new JButton("Modificar");
		btnModificar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				ModificarUsuario modificar = new ModificarUsuario();

				modificar.setVisible(true);

			}
		});
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnModificar.setBounds(60, 60, 89, 23);
		contentPane.add(btnModificar);

		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				EliminarUsuario eliminar = new EliminarUsuario();

				eliminar.setVisible(true);

			}
		});
		btnEliminar.setBounds(60, 94, 89, 23);
		contentPane.add(btnEliminar);

		JButton btnConsultar = new JButton("Consultar");
		btnConsultar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				ConsultarUsuario Consulta = new ConsultarUsuario();
				Consulta.setVisible(true);

			}
		});
		btnConsultar.setBounds(60, 135, 89, 23);
		contentPane.add(btnConsultar);

		JButton btnRegresar = new JButton("Regresar");
		btnRegresar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				Main principal = new Main();
			principal.setVisible(true);

			}
		});
		btnRegresar.setBounds(10, 223, 77, 23);
		contentPane.add(btnRegresar);
	}
}