package Administrador;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import EDU.purdue.cs.bloat.decorate.Main;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;
import java.awt.TextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AgregarUsuario extends JFrame {

	private JPanel contentPane;
	private JTextField txtID;
	private JTextField txtNom;
	private JTextField txtTarjeta;
	private JTextField txtNip;
	private JTextField txtFondos;
	TextArea textArea = new TextArea();
	
	Main menuu = new Main ();

	Usuario user = new Usuario();
	MetodosAdmin metodos = new MetodosAdmin();
	Usuario usuarios = new Usuario();
	

	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					AgregarUsuario frame = new AgregarUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Create the frame.
	 */
	public AgregarUsuario() {

		int resp = JOptionPane.showConfirmDialog(null, "�Eres mayor de edad?");

		if (JOptionPane.YES_NO_OPTION == resp) {

		} else {
			JOptionPane.showInputDialog("Agregue nombre de tutor");
		}
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 395, 453);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Nombre:");
		lblNewLabel.setBounds(21, 91, 86, 14);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_2 = new JLabel("NIP: ");
		lblNewLabel_2.setBounds(21, 121, 46, 14);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Fondo Inicial:");
		lblNewLabel_3.setBounds(21, 146, 98, 14);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("ID:");
		lblNewLabel_4.setBounds(21, 66, 46, 14);
		contentPane.add(lblNewLabel_4);

		txtID = new JTextField();
		txtID.setBounds(129, 63, 114, 20);
		contentPane.add(txtID);
		txtID.setColumns(10);

		txtNom = new JTextField();
		txtNom.setBounds(129, 88, 114, 20);
		contentPane.add(txtNom);
		txtNom.setColumns(10);

		txtTarjeta = new JTextField();
		txtTarjeta.setBounds(129, 169, 114, 20);
		contentPane.add(txtTarjeta);
		txtTarjeta.getColumns();
		txtTarjeta.setEnabled(false);
		
		

		JLabel lblNewLabel_1 = new JLabel("Numero de Tarjeta:");
		lblNewLabel_1.setBounds(21, 172, 121, 14);
		contentPane.add(lblNewLabel_1);
		

		txtNip = new JTextField();
		txtNip.setBounds(129, 118, 114, 20);
		contentPane.add(txtNip);
		txtNip.setColumns(10);

		txtFondos = new JTextField();
		txtFondos.setBounds(129, 143, 114, 20);
		contentPane.add(txtFondos);
		txtFondos.setColumns(10);

		JButton btnGuardar = new JButton("Agregar");
		// usuarios.verificarEdad();

		btnGuardar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				usuarios.setIdUsuario(Integer.parseInt(txtID.getText()));
				usuarios.setNombreUsuario(txtNom.getText());
				usuarios.getNumTarjeta();
				usuarios.setNip(Integer.parseInt(txtNip.getText()));
				usuarios.setFondoInicial(Double.parseDouble(txtFondos.getText()));
				metodos.insertarRegistro(usuarios);

				txtID.setText("");
				txtNom.setText("");
				txtTarjeta.setText("");
				txtNip.setText("");
				txtFondos.setText("");

				JOptionPane.showMessageDialog(null, "Datos Guardados: \n " + usuarios, getTitle(),
						JOptionPane.WARNING_MESSAGE);
				
				textArea.append(usuarios.toString());
				
				
			}

		});
		btnGuardar.setBounds(276, 62, 89, 23);
		contentPane.add(btnGuardar);

		JLabel lblNewLabel_5 = new JLabel("Nuevo usuario");
		lblNewLabel_5.setFont(new Font("Arial", Font.PLAIN, 17));
		lblNewLabel_5.setBounds(129, 11, 121, 41);
		contentPane.add(lblNewLabel_5);

		JButton btnRegresar = new JButton("Atras");
		btnRegresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnRegresar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				Administrador principal = new Administrador();
				principal.setVisible(true);

			}
		});
		btnRegresar.setBounds(30, 375, 89, 23);
		contentPane.add(btnRegresar);
		textArea.setEditable(false);
		
		textArea.setBounds(67, 210, 232, 145);
		contentPane.add(textArea);
		


	}

	
	
}