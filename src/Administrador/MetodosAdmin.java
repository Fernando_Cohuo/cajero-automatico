package Administrador;


import java.util.ArrayList;
import java.util.List;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;


public class MetodosAdmin {

	private ObjectContainer db=null;
	 
	 private void abrirRegistro() {
		 
		 
		 //abrirConexion
		 
	 db=Db4oEmbedded.openFile("registroUsuario");
			 }
	 
	//CERRAR BASE DE DATOS
	 private void cerrarRegistro() {
	
		 db.close();
		 
	 }
	 
	 

	 //INSERTA USUARIO A LA BASE DE DATOS
	public void insertarRegistro(Usuario usuario) {

        abrirRegistro();
		
        db.store(usuario);
		cerrarRegistro();
		
	}
	
		//ARRAYLIST  DE LOS USUARIOS
	public List<Usuario> seleccionarUsuarios(){
		abrirRegistro();
		ObjectSet listaUsuarios=db.queryByExample(Usuario.class);
		List<Usuario> lp =new ArrayList<>();
		for (Object listaUsuarios1:listaUsuarios) {
			lp.add((Usuario)listaUsuarios1);
			
		}
		cerrarRegistro();
		return lp;
		
	}

	//CONSULTA DE USUARIOS
	
	public Usuario seleccionarUsuario(Usuario usuarios) {
		abrirRegistro();
		ObjectSet resultado=db.queryByExample(usuarios);
		Usuario usuario=(Usuario) resultado.next();
		cerrarRegistro();
		return usuario;
		
	}
	//METODO DE ACTUALIZAR

	public void actualizarRegistro(int id,String nombre,Double fondoInicial,int numTarjeta, int nip) {

		abrirRegistro();
		Usuario p = new Usuario();
		p.setNumTarjeta(numTarjeta);
		ObjectSet resultado=db.queryByExample(p);
		Usuario auxiliar=(Usuario) resultado.next();
		auxiliar.setIdUsuario(id);
		auxiliar.setNombreUsuario(nombre);
		auxiliar.setFondoInicial(fondoInicial);
		auxiliar.setNumTarjeta(numTarjeta);
		auxiliar.setNip(nip);
		db.store(auxiliar);
		cerrarRegistro();
	}
	
	//METODO DE ELIMINAR

	public Usuario eliminarRegistros(int id) {
		abrirRegistro();
		Usuario usuario = new Usuario();
		usuario.setIdUsuario(id);
		ObjectSet resultado=db.queryByExample(usuario);
		Usuario auxiliar=(Usuario) resultado.next();
		
		//Metodo de eliminar 
		db.delete(auxiliar);
		cerrarRegistro();
		return auxiliar;
		
		
	}

	
	
}
